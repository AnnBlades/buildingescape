// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "Components/InputComponent.h"
#include "Grabber.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabber();
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	FVector GetReachLineEnd();
	FVector GetReachLineStart();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	void SetupInputHandleComponent();

	void FindPhysicsHandleComponent();

	
private:
	float reach=100.0f;

	UPhysicsHandleComponent* physicsHandle = nullptr;
	UInputComponent* inputComopnent = nullptr;
	FHitResult GetFirstPhysicsBodyInReach();

	void Grab();
	void Release();

};


