// Fill out your copyright notice in the Description page of Project Settings.

#include "OpenDoor.h"
//#include "Components/StaticMeshComponent.h" 
// Sets default values for this component's properties
#define OUT


UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();
	if (!pressurePlate)
	{
		UE_LOG(LogTemp, Error, TEXT("%s loose pressurePlate"),*GetOwner()->GetName());
	}
}


// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	//Poll the trigger Volume
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (GetTotalMassOfActorsOnPlate()>triggerMass)//TODO make into parametr
	{
		OnOpen.Broadcast();
	}
	else
	{
		OnClose.Broadcast();
	}
}

float UOpenDoor::GetTotalMassOfActorsOnPlate()
{
	float totalMass = 0.0f;
	//find all overlapping actors;
	
	TArray<AActor*> OverlapingActors;
	if (!pressurePlate) { return totalMass; }
	pressurePlate->GetOverlappingActors(OUT OverlapingActors);
	//iterate them
	for (auto* actor : OverlapingActors)
	{
		totalMass+=actor->FindComponentByClass<UPrimitiveComponent>()->GetMass();
		UE_LOG(LogTemp,Warning,TEXT("%s on pressure plate"), *actor->GetName())
	}
	return totalMass;
}