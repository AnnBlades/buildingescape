// Fill out your copyright notice in the Description page of Project Settings.
#include "Grabber.h"
#include "DrawDebugHelpers.h"
#define OUT

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();

	///Looks for attached Physics Handle
	FindPhysicsHandleComponent();
	SetupInputHandleComponent();
}



// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (!physicsHandle) { return; }
	//if the physic handle found
	if (physicsHandle->GrabbedComponent)
	{
		//move the object that we holding every frame
		physicsHandle->SetTargetLocation(GetReachLineEnd());
	}
	
}


void UGrabber::SetupInputHandleComponent()
{
	inputComopnent = GetOwner()->FindComponentByClass<UInputComponent>();
	if (inputComopnent)
	{
		inputComopnent->BindAction("Grab", IE_Pressed, this, &UGrabber::Grab);
		inputComopnent->BindAction("Grab", IE_Released, this, &UGrabber::Release);

	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("%s has no Input handle"), *(GetOwner()->GetName()));

	}
}

void UGrabber::FindPhysicsHandleComponent()
{
	physicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	if (physicsHandle==nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("%s has no Physics handle"), *(GetOwner()->GetName()));
	}
}

void UGrabber::Grab() {
	//Line Trace and see if we reach any actors with physics bode collision channel set
	auto hitresult=GetFirstPhysicsBodyInReach();
	auto componentToGrab = hitresult.GetComponent();//get the mesh from our case
	auto actorhit = hitresult.GetActor();
	
	//if we hit something then attach a physics handle
	if (actorhit)
	{
		if (!physicsHandle) { return; }
		physicsHandle->GrabComponent(
			componentToGrab,
			NAME_None,//no bones needed
			componentToGrab->GetOwner()->GetActorLocation(),
			true//allow rotation
		);
	}
}

void UGrabber::Release()
{
	if (!physicsHandle) { return; }
	physicsHandle->ReleaseComponent();

}

FHitResult UGrabber::GetFirstPhysicsBodyInReach()
{
	
	
	//Ray-cast out to reach distance
	FHitResult hitResult;
	FCollisionQueryParams TraceParameters(FName(TEXT("")), false, GetOwner());

	GetWorld()->LineTraceSingleByObjectType(
		OUT hitResult,
		GetReachLineStart(),
		GetReachLineEnd(),
		FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
		TraceParameters
	);

	return hitResult;
}

FVector UGrabber::GetReachLineEnd()
{
	FVector PlayerViewPointLocation;
	FRotator PlayerViewPointRotation;
	FVector reachlineend;
	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OUT PlayerViewPointLocation,
		OUT PlayerViewPointRotation);
	reachlineend = PlayerViewPointLocation + PlayerViewPointRotation.Vector()*reach;
	return reachlineend;
	
}

FVector UGrabber::GetReachLineStart()
{
	FVector PlayerViewPointLocation;
	FRotator PlayerViewPointRotation;
	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OUT PlayerViewPointLocation,
		OUT PlayerViewPointRotation);
	return PlayerViewPointLocation;
}
